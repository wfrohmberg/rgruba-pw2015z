﻿using System.Windows;
using System.Windows.Media;
using GeneticSolver.UI.ViewModels;

namespace GeneticSolver.UI.Views
{
    /// <summary>
    /// Interaction logic for SolveView.xaml
    /// </summary>
    public partial class SolveView : Window
    {

        public SolveView()
        {
            InitializeComponent();
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);
            var solveViewModel = DataContext as SolveViewModel;
            if (solveViewModel != null) solveViewModel.DrawingArea = DrawingPanel;
        }
    }
}
