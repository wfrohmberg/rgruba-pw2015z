﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;

namespace GeneticSolver.UI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        private BootstrapperBase _bootstrapper;
        public App()
        {
            _bootstrapper = new Bootstrapper();
        }
    }
}
