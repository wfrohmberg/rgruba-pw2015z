﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Documents;
using Caliburn.Micro;
using GeneticSolver.UI.ViewModels;

namespace GeneticSolver.UI
{
    public class Bootstrapper : BootstrapperBase
    {
        public Bootstrapper()
        {
            Initialize();
            if (Debugger.IsAttached)
                LogManager.GetLog = type => new DebugLogger(type);
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<SolveViewModel>();
        }

        protected override IEnumerable<Assembly> SelectAssemblies()
        {
            var assemblies = new List<Assembly>();
            assemblies.AddRange(base.SelectAssemblies());
            string[] fileEntries = Directory.GetFiles(Directory.GetCurrentDirectory(),"*.dll");
            //TODO check for attribute?
            assemblies.AddRange(fileEntries.Select(Assembly.LoadFile));
            return assemblies;
        }
    }
}