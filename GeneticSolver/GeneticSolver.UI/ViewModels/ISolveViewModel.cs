﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using GenericSolver.UI.Common;

namespace GeneticSolver.UI.ViewModels
{
    public interface ISolveViewModel
    {
        IList<IProblemViewModel> Problems { get; set; }

        IProblemViewModel SelectedProblem { get; set; }

        Canvas DrawingArea { get; set; }

        bool IsBusy { get; set; }

        bool CanSaveProblem { get; set; }

        bool CanReadProblem { get; set; }

        ICommand ReadProblemStateCommand { get; }

        ICommand SaveProblemStateCommand { get; }
    }
}