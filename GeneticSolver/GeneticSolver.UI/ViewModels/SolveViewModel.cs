﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reactive.Concurrency;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using Caliburn.Micro;
using GenericSolver.UI.Common;
using GeneticSolver.Tsp.ViewModels;
using Ookii.Dialogs.Wpf;

namespace GeneticSolver.UI.ViewModels
{
    public class SolveViewModel : Conductor<object>, ISolveViewModel
    {
        private readonly CompositeDisposable _subsriptions = new CompositeDisposable();
        private const string Filter = "Text files (*.txt)|*.txt|Json files (*.json)|*.json";
        //TODO search for problems in directory need method in directory extenstions
        public SolveViewModel()
        {
            _problems = new ObservableCollection<IProblemViewModel> { new TspProblemViewModel() };
            SaveProblemStateCommand = new DelegateCommand(_ => SaveProblemState(), _ => CanSaveProblem);
            ReadProblemStateCommand = new DelegateCommand(_ => ReadProblemState(), _ => CanReadProblem);

        }

        private bool _canReadProblem;

        public bool CanReadProblem
        {
            get { return _canReadProblem; }
            set
            {
                if (value.Equals(_canReadProblem)) return;
                _canReadProblem = value;
                NotifyOfPropertyChange(() => CanReadProblem);
            }
        }

        private bool _canSaveProblem;

        public bool CanSaveProblem
        {
            get { return _canSaveProblem; }
            set
            {
                if (value.Equals(_canSaveProblem)) return;
                _canSaveProblem = value;
                NotifyOfPropertyChange(() => CanSaveProblem);
            }
        }

        private void ReadProblemState()
        {
            if (SelectedProblem == null)
                return;
            var dialog = new VistaOpenFileDialog()
            {
                Filter = Filter
            };
            var res = dialog.ShowDialog();
            if (res.HasValue && res.Value)
            {
                IsBusy = true;
                SelectedProblem.ReadProblemState(dialog.FileName)
                    .ObserveOn(DispatcherScheduler.Current)
                    .SubscribeOn(ThreadPoolScheduler.Instance)
                    .Subscribe(problemState =>
                    {
                        SelectedProblem.RestoreProblemState(problemState);
                        IsBusy = false;
                    });
            }
        }

        private void SaveProblemState()
        {
            if (SelectedProblem == null)
                return;
            //TODO need to wrap this in some class
            var fileName = CreateFileName();
            var dialog = new VistaSaveFileDialog
            {
                FileName = fileName,
                DefaultExt = "json",
                AddExtension = true,
                Filter = Filter
            };
            var res = dialog.ShowDialog();
            if (res.HasValue && res.Value)
            {
                IsBusy = true;
                SelectedProblem.SaveProblemState(dialog.FileName)
                    .ObserveOn(DispatcherScheduler.Current)
                    .SubscribeOn(ThreadPoolScheduler.Instance)
                    .Subscribe(_ => IsBusy = false);
            }
        }

        private string CreateFileName()
        {
            return SelectedProblem.SelectedAlgorithm != null
                ? string.Format("{0}_{1}_{2}", SelectedProblem.ProblemName,
                    SelectedProblem.SelectedAlgorithm.AlgorithmName, DateTime.UtcNow.Ticks)
                : string.Format("{0}_{1}", SelectedProblem.ProblemName, DateTime.UtcNow.Ticks);
        }

        private IList<IProblemViewModel> _problems;

        public IList<IProblemViewModel> Problems
        {
            get { return _problems; }
            set
            {
                if (Equals(value, _problems)) return;
                _problems = value;
                NotifyOfPropertyChange(() => Problems);
            }
        }

        private IProblemViewModel _selectedProblem;

        public IProblemViewModel SelectedProblem
        {
            get { return _selectedProblem; }
            set
            {
                if (Equals(value, _selectedProblem)) return;
                _selectedProblem = value;
                _selectedProblem.DrawingArea = DrawingArea;
                _subsriptions.Clear();
                _subsriptions.Add(_selectedProblem.IsBusyStream.ObserveOn(DispatcherScheduler.Current).Subscribe(v => IsBusy = v));
                _subsriptions.Add(_selectedProblem.CanSaveStream.ObserveOn(DispatcherScheduler.Current).Subscribe(v =>
                        {
                            CanSaveProblem = v;
                            ((DelegateCommand)SaveProblemStateCommand).RaiseCanExecuteChanged();
                        }));

                _subsriptions.Add(_selectedProblem.CanReadStream.ObserveOn(DispatcherScheduler.Current).Subscribe(v =>
                        {
                            CanReadProblem = v;
                            ((DelegateCommand)ReadProblemStateCommand).RaiseCanExecuteChanged();
                        }));
                NotifyOfPropertyChange(() => SelectedProblem);
                ActivateItem(value);
            }
        }

        private Canvas _drawingArea;

        public Canvas DrawingArea
        {
            get { return _drawingArea; }
            set
            {
                _drawingArea = value;
                if (SelectedProblem != null)
                    SelectedProblem.DrawingArea = value;
            }
        }

        private bool _isBusy;

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                if (value.Equals(_isBusy)) return;
                _isBusy = value;
                NotifyOfPropertyChange(() => IsBusy);
            }
        }

        public ICommand ReadProblemStateCommand { get; private set; }

        public ICommand SaveProblemStateCommand { get; private set; }
    }
}