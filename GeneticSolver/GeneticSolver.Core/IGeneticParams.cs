﻿using GeneticSolver.Core.Problem;

namespace GeneticSolver.Core
{
    public interface IGeneticParams<T,TK> : IAlgorithmParams where T : IProblemInstance
    {
        IRandomSoultionGenerator<T,TK> SolutionGenerator { get; set; }

        ISelection<T, TK> Selection { get; set; }

        ICrossingOperator<T, TK> Crossing { get; set; }

        IMutationOperator<T, TK> MutationOperator { get; set; }

        int SizeOfPopulation { get; set; }

        int NumberOfGenerations { get; set; }

        double CrossingProbability { get; set; }

        double MutationProbability { get; set; }

    }

    public class GeneticParams<T, TK> : IGeneticParams<T, TK> where T : IProblemInstance
    {
        public IRandomSoultionGenerator<T, TK> SolutionGenerator { get; set; }

        public ISelection<T, TK> Selection { get; set; }

        public ICrossingOperator<T, TK> Crossing { get; set; }

        public IMutationOperator<T, TK> MutationOperator { get; set; }

        public int SizeOfPopulation { get; set; }

        public int NumberOfGenerations { get; set; }

        public double CrossingProbability { get; set; }

        public double MutationProbability { get; set; }

    }
}