﻿using System;
using GeneticSolver.Core.Problem;

namespace GeneticSolver.Core
{
    public interface IMutationOperator<T,TK> where T : IProblemInstance
    {
        void Mutate(IProblemSolution<T,TK> problemSolution, IRandomWrapper randomizer);
    }
}