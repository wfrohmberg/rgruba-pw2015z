﻿using System;
using System.Collections.Generic;

namespace GeneticSolver.Core.Extenstions
{
    public static class EnumerableExtenstions
    {
        public static void ForEach<T>(this IEnumerable<T> collections, Action<T> action)
        {
            foreach (var element in collections)
            {
               action(element);
            }
        }
    }
}