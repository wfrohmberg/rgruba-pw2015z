﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace GeneticSolver.Core.Extenstions
{
    public static class AssemblyExtenstions
    {
        public static IEnumerable<T> CreateInstancesOfTypesWhichImplements<T>(this Assembly assembly) where T : class
        {
            return assembly.GetTypes()
                .Where(x => x.GetInterfaces().Contains(typeof (T)))
                .Select(t=> Activator.CreateInstance(t) as T);
        }
    }
}