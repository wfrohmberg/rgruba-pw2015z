﻿namespace GeneticSolver.Core
{
    public interface IRandomWrapper
    {
        double NextDouble();

        int Next(int maxValue);

        int Next(int minValue, int maxValue);
    }
}