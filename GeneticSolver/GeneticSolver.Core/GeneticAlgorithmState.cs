﻿using System.Collections.Generic;
using System.Windows.Documents;

namespace GeneticSolver.Core
{
    public class GeneticAlgorithmState<TK> : IAlgoritmState
    {
        public int CurrentGeneration { get; set; }

        public List<TK> Population { get; set; }
    }
}