using System;
using GeneticSolver.Core.Problem;

namespace GeneticSolver.Core
{
    public interface IAlgorithm
    {
        IObservable<IProblemSolution> GetBestSolution(IAlgorithmParams algorithmParams,IProblemInstance problemInstance);

        IObservable<AlgoritmState> StateStream { get; } 

        string AlgorithmName { get; }

        AlgoritmState State { get; }

        void PauseSolving();

        void StopSolving();

        string SerializeParams(IAlgorithmParams algorithmParams);

        string SerializeState();

        IAlgorithmParams DeserializeParams(string algorithmParams);

        void DeserializeState(string algorithmState, object bestSolution, object problemInstance);

        void UpdateParams(IAlgorithmParams algorithmParams);
    }

    public interface IAlgorithm<T, TK> : IAlgorithm where T : IProblemInstance
    {

        IObservable<IProblemSolution<T, TK>> GetBestSolution(IAlgorithmParams algorithmParams, T problemInstance);

        IObservable<IProblemSolution<T, TK>> RestartSolving();
    } 
}