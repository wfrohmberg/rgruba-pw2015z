﻿namespace GeneticSolver.Core
{
    public enum AlgoritmState
    {
        Initialized,
        Started,
        Paused,
        Completed
    }
}