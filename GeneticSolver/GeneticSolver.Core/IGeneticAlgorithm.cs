using System;
using GeneticSolver.Core.Problem;

namespace GeneticSolver.Core
{
    public interface IGeneticAlgorithm : IAlgorithm
    {
    }

    public interface IGeneticAlgorithm<T, TK> : IGeneticAlgorithm, IAlgorithm<T, TK> where T : IProblemInstance
    {
        new IObservable<IProblemSolution<T, TK>> GetBestSolution(IAlgorithmParams algorithmParams, T problemInstance);

        IRandomSoultionGenerator<T, TK> SolutionGenerator { get; }

        ISelection<T, TK> Selection { get; }

        ICrossingOperator<T, TK> Crossing { get; }

        IMutationOperator<T, TK> MutationOperator { get; }

        int SizeOfPopulation { get; }

        int NumberOfGenerations { get; }

        double CrossingProbability { get; }

        double MutationProbability { get; }

        int CurrentGeneration { get; }
    }

}