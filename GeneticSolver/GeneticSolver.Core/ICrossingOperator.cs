﻿using System;
using System.Collections.Generic;
using GeneticSolver.Core.Problem;

namespace GeneticSolver.Core
{
    public interface ICrossingOperator<T,TK> where T : IProblemInstance
    {
        IList<IProblemSolution<T,TK>> MakeChildren(IProblemSolution<T,TK> father, IProblemSolution<T,TK> mother, IRandomWrapper randomizer);

        string Name { get; }
    }
}