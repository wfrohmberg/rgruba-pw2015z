﻿using System;
using GeneticSolver.Core.Problem;
using Newtonsoft.Json;

namespace GeneticSolver.Core
{
    public class GeneticAlgorithmSerializer<T, TK> : IAlgorithmSerializer where T : IProblemInstance
    {
        private class GeneticAlgorithmParamsDto
        {
            public string SolutionGeneratorType { get; set; }

            public string SelectionType { get; set; }

            public string CrossingType { get; set; }

            public string MutationOperatorType { get; set; }

            public int SizeOfPopulation { get; set; }

            public int NumberOfGenerations { get; set; }

            public double CrossingProbability { get; set; }

            public double MutationProbability { get; set; }
        }

        public string SerializeParams(IAlgorithmParams algorithmParams)
        {
            var par = algorithmParams as IGeneticParams<T, TK>;
            if (par == null)
                throw new ArgumentNullException("algorithmParams");

            var dto = new GeneticAlgorithmParamsDto
            {
                SolutionGeneratorType = par.SolutionGenerator != null ? par.SolutionGenerator.GetType().FullName : string.Empty,
                SelectionType = par.Selection != null ? par.Selection.GetType().FullName : string.Empty,
                CrossingType = par.Crossing != null ? par.Crossing.GetType().FullName : string.Empty,
                MutationOperatorType = par.MutationOperator != null ? par.MutationOperator.GetType().FullName : string.Empty,
                SizeOfPopulation = par.SizeOfPopulation,
                NumberOfGenerations = par.NumberOfGenerations,
                CrossingProbability = par.CrossingProbability,
                MutationProbability = par.MutationProbability
            };

            return JsonConvert.SerializeObject(dto);

        }

        public IAlgorithmParams DeserializeParams(string algorithmParams)
        {
            var paramsDto = JsonConvert.DeserializeObject<GeneticAlgorithmParamsDto>(algorithmParams);
            var par = new GeneticParams<T, TK>
            {
                SizeOfPopulation = paramsDto.SizeOfPopulation,
                NumberOfGenerations = paramsDto.NumberOfGenerations,
                CrossingProbability = paramsDto.CrossingProbability,
                MutationProbability = paramsDto.MutationProbability
            };

            if (!string.IsNullOrEmpty(paramsDto.SolutionGeneratorType))
            {
                par.SolutionGenerator = (IRandomSoultionGenerator<T, TK>) typeof (T).Assembly.CreateInstance(paramsDto.SolutionGeneratorType);
            }

            if (!string.IsNullOrEmpty(paramsDto.SelectionType))
            {
                par.Selection = (ISelection<T, TK>) typeof(T).Assembly.CreateInstance(paramsDto.SelectionType);
            }

            if (!string.IsNullOrEmpty(paramsDto.CrossingType))
            {
                par.Crossing = (ICrossingOperator<T, TK>) typeof(T).Assembly.CreateInstance(paramsDto.CrossingType);
            }

            if (!string.IsNullOrEmpty(paramsDto.MutationOperatorType))
            {
                par.MutationOperator = (IMutationOperator<T, TK>) typeof(T).Assembly.CreateInstance(paramsDto.MutationOperatorType);
            }

            return par;
        }

        public string SerializeState(IAlgoritmState algorithmState)
        {
            var state = algorithmState as GeneticAlgorithmState<TK>;
            if (state == null)
                throw new ArgumentNullException("algorithmState");
            return JsonConvert.SerializeObject(state);
        }

        public IAlgoritmState DeserializeState(string algorithmState)
        {
            return JsonConvert.DeserializeObject<GeneticAlgorithmState<TK>>(algorithmState);
        }
    }
}