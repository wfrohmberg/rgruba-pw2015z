﻿using GeneticSolver.Core.Problem;

namespace GeneticSolver.Core
{
    public interface IRandomSoultionGenerator<T,TK> where T : IProblemInstance
    {
        IProblemSolution<T, TK> Generate(T instance, IRandomWrapper randomizer);

        IProblemSolution<T, TK> RestoreSolution(T instance, TK result);
    }
}