using System;

namespace GeneticSolver.Core
{
    public class RandomWrapper : IRandomWrapper
    {
        private readonly Random _randomizer;

        public RandomWrapper()
        {
            _randomizer = new Random();
        }

        private RandomWrapper(int seed)
        {
            _randomizer = new Random(seed);
        }

        public double NextDouble()
        {
            return _randomizer.NextDouble();
        }

        public int Next(int maxValue)
        {
            return _randomizer.Next(maxValue);
        }

        public int Next(int minValue, int maxValue)
        {
            return _randomizer.Next(minValue, maxValue);
        }
    }
}