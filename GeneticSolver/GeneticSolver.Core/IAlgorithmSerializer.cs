﻿namespace GeneticSolver.Core
{
    public interface IAlgorithmSerializer
    {
        string SerializeParams(IAlgorithmParams algorithmParams);

        IAlgorithmParams DeserializeParams(string algorithmParams);

        string SerializeState(IAlgoritmState algorithmState);

        IAlgoritmState DeserializeState(string algorithmState);
    }
}