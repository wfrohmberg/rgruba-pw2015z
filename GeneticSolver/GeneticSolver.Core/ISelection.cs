﻿using System;
using System.Collections.Generic;
using GeneticSolver.Core.Problem;

namespace GeneticSolver.Core
{
    public interface ISelection<T, TK> where T : IProblemInstance
    {
        IList<IProblemSolution<T, TK>> CreateNewGeneration(IList<IProblemSolution<T, TK>> population, IRandomWrapper randomizer);

        string Name { get; }
    }
}