﻿namespace GeneticSolver.Core.Problem
{
    public interface IProblemInstanceGenerator<out T> where T : IProblemInstance
    {
        T Generate(IProblemParams parameters);
    }
}