﻿namespace GeneticSolver.Core.Problem
{
    public interface IProblemSerializer<in TP> where TP : IProblem
    {
        string Serialize(TP problem, IAlgorithm algorithm, IAlgorithmParams algorithmParams);
    }
}