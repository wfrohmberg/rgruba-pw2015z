﻿using System;

namespace GeneticSolver.Core.Problem
{
    public interface IProblemSolution
    {
        object SolutionValue { get; }

        object Instance { get; }

        void Evaluate();
    }

    public interface IProblemSolution<T, TK> :IProblemSolution, IComparable<IProblemSolution<T, TK>> where T : IProblemInstance
    {
        new TK SolutionValue { get; }

        new T Instance { get; }

        bool IsBetterThan(IProblemSolution<T, TK> problemSolution);
    }
}