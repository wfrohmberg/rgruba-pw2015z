﻿namespace GeneticSolver.Core.Problem
{
    public abstract class ProblemSolution<T,TK> : IProblemSolution<T,TK> where T : IProblemInstance
    {
        protected ProblemSolution(T instance, TK solutionValue)
        {
            Instance = instance;
            SolutionValue = solutionValue;
        }
        
        public T Instance { get; private set; }

        public abstract void Evaluate();

        public TK SolutionValue { get; private set; }

        object IProblemSolution.SolutionValue
        {
            get { return SolutionValue; }
        }

        object IProblemSolution.Instance
        {
            get { return Instance; }
        }

        public bool IsBetterThan(IProblemSolution<T, TK> problemSolution)
        {
            return IsBetter(problemSolution);
        }

        public int CompareTo(IProblemSolution<T, TK> other)
        {
            return Compare(other);
        }

        protected abstract int Compare(IProblemSolution<T, TK> other);

        protected abstract bool IsBetter(IProblemSolution<T, TK> problemSolution);
    }
}