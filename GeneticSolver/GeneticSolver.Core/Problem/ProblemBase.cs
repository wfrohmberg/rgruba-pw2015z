﻿using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Windows.Controls;

namespace GeneticSolver.Core.Problem
{
    public abstract class ProblemBase : IProblem
    {
        private readonly ISchedulerProvider _schedulerProvider;

        protected ProblemBase(string problemName) : this(problemName,new SchedulerProvider())
        {
            
        }

        protected ProblemBase(string problemName, ISchedulerProvider schedulerProvider)
        {
            ProblemName = problemName;
            _schedulerProvider = schedulerProvider;
        }

        protected abstract IProblemInstance GenerateInstance(IProblemParams problemParams);

        public IObservable<IProblemInstance> GenerateProblemInstance(IProblemParams problemParams)
        {
            return Observable.Start(() => GenerateInstance(problemParams), _schedulerProvider.CurrentThread);
        }

        public abstract void DrawInstance(IProblemInstance instance, Canvas drawingArea);

        public string ProblemName { get; private set; }

        public abstract void DrawSolution(IProblemSolution solution, Canvas drawingArea);

        public IProblemInstance ProblemInstance { get; set; }

        public IProblemSolution CurrentSolution { get; set; } 
    }
}