﻿namespace GeneticSolver.Core.Problem
{
    public interface IProblemDto
    {
        string AlgorithmName { get; set; }

        string AlgorithmParams { get; set; }

        string AlgorithmState { get; set; }


    }
}