﻿namespace GeneticSolver.Core.Problem
{
    public interface IProblemDeserializer<out TP> where TP : IProblem
    {
        IProblemDto Deserialize(string jsonString);

        IProblem CreateProblemFromDto(IProblemDto dto);
    }
}