﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;

namespace GeneticSolver.Core.Problem
{
    public interface IProblem
    {
        IObservable<IProblemInstance> GenerateProblemInstance(IProblemParams problemParams);

        void DrawInstance(IProblemInstance instance, Canvas drawingArea);

        void  DrawSolution(IProblemSolution solution, Canvas drawingArea);

        string ProblemName { get; }
    }
}