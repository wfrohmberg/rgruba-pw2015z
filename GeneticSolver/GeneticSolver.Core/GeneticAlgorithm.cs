﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading;
using GeneticSolver.Core.Problem;

namespace GeneticSolver.Core
{
    public class GeneticAlgorithm<T, TK> : IGeneticAlgorithm<T, TK> where T : IProblemInstance
    {
        public IRandomSoultionGenerator<T, TK> SolutionGenerator { get; private set; }

        public ISelection<T, TK> Selection  { get; private set; }

        public ICrossingOperator<T, TK> Crossing  { get; private set; }

        public IMutationOperator<T, TK> MutationOperator { get; private set; }

        public int SizeOfPopulation { get; private set; }

        public int NumberOfGenerations { get; private set; }

        public double CrossingProbability { get; private set; }

        public double MutationProbability { get; private set; }

        public int CurrentGeneration { get; private set; }

        private IList<IProblemSolution<T, TK>> _population;

        private readonly ISubject<AlgoritmState> _stateSubject = new BehaviorSubject<AlgoritmState>(AlgoritmState.Initialized);

        private ISubject<IProblemSolution<T, TK>> _solutionSubject;

        private readonly IRandomWrapper _randomizer;
        private readonly ISchedulerProvider _schedulerProvider;

        private readonly IAlgorithmSerializer _serializer = new GeneticAlgorithmSerializer<T, TK>();

        public GeneticAlgorithm(string algorithmName)
            : this(algorithmName, new RandomWrapper(), new SchedulerProvider())
        {
        }

        public GeneticAlgorithm(string algorithmName, IRandomWrapper randomizer, ISchedulerProvider schedulerProvider)
        {
            AlgorithmName = algorithmName;
            _randomizer = randomizer;
            _schedulerProvider = schedulerProvider;
        }

        private volatile AlgoritmState _state = AlgoritmState.Initialized;
        private IProblemSolution<T, TK> _bestSolution;

        public string AlgorithmName { get; private set; }

        public AlgoritmState State
        {
            get { return _state; }
            protected set
            {
                _state = value;
                _stateSubject.OnNext(value);
            }
        }

        public void PauseSolving()
        {
            State = AlgoritmState.Paused;
        }

        public IObservable<IProblemSolution<T, TK>> RestartSolving()
        {
            _solutionSubject = new Subject<IProblemSolution<T, TK>>();
            State = AlgoritmState.Started;
            Observable.Create<IProblemSolution<T, TK>>(observer => GenerateGenerations(observer))
                .SubscribeOn(_schedulerProvider.ThreadPool).Subscribe(solution => _solutionSubject.OnNext(solution));

            return _solutionSubject.AsObservable();
        }

        public void StopSolving()
        {
            State = AlgoritmState.Completed;
            if (_solutionSubject != null)
                _solutionSubject.OnCompleted();
        }

        public string SerializeParams(IAlgorithmParams algorithmParams)
        {
            return _serializer.SerializeParams(algorithmParams);
        }

        public string SerializeState()
        {
            var state = new GeneticAlgorithmState<TK>
            {
                CurrentGeneration = CurrentGeneration,
                Population = _population.Select(x => x.SolutionValue).ToList()
            };
            return _serializer.SerializeState(state);
        }

        public IAlgorithmParams DeserializeParams(string algorithmParams)
        {
            return _serializer.DeserializeParams(algorithmParams);
        }

        public void DeserializeState(string algorithmState, object bestSolution, object problemInstance)
        {
            var state = (GeneticAlgorithmState<TK>)_serializer.DeserializeState(algorithmState);
            CurrentGeneration = state.CurrentGeneration;
            _bestSolution = (IProblemSolution<T, TK>) bestSolution;
            _population = new List<IProblemSolution<T, TK>>();
            foreach (var sol in state.Population)
            {
                _population.Add(SolutionGenerator.RestoreSolution((T)problemInstance, sol));
            }
            if(bestSolution == null)
                return;
            _bestSolution = (IProblemSolution<T, TK>) bestSolution;
            PauseSolving();
        }

        public void UpdateParams(IAlgorithmParams algorithmParams)
        {
            var par = (IGeneticParams<T, TK>) algorithmParams;
            Crossing = par.Crossing;
            CrossingProbability = par.CrossingProbability;
            MutationOperator = par.MutationOperator;
            MutationProbability = par.MutationProbability;
            NumberOfGenerations = par.NumberOfGenerations;
            Selection = par.Selection;
            SizeOfPopulation = par.SizeOfPopulation;
            SolutionGenerator = par.SolutionGenerator;
        }

        private IList<IProblemSolution<T, TK>> CreateRandomPopulation(T problemInstance)
        {
            var population = new List<IProblemSolution<T, TK>>(SizeOfPopulation);
            for (int i = 0; i < SizeOfPopulation; i++)
            {
                population.Add(SolutionGenerator.Generate(problemInstance, _randomizer));
            }

            return population;
        }

        public IObservable<IProblemSolution<T, TK>> GetBestSolution(IAlgorithmParams algorithmParams, T problemInstance)
        {
            _solutionSubject = new Subject<IProblemSolution<T, TK>>();
            State = AlgoritmState.Started;
            Observable.Create<IProblemSolution<T, TK>>(observer =>
                {
                    var par = (IGeneticParams<T, TK>)algorithmParams;
                    SolutionGenerator = par.SolutionGenerator;
                    Selection = par.Selection;
                    Crossing = par.Crossing;
                    MutationOperator = par.MutationOperator;
                    SizeOfPopulation = par.SizeOfPopulation;
                    NumberOfGenerations = par.NumberOfGenerations;
                    CrossingProbability = par.CrossingProbability;
                    MutationProbability = par.MutationProbability;

                    _population = CreateRandomPopulation(problemInstance);

                    _bestSolution = _population.Min(x => x);
                    observer.OnNext(_bestSolution);
                    CurrentGeneration = 0;
                    return GenerateGenerations(observer);
                }).SubscribeOn(_schedulerProvider.ThreadPool)
                .Subscribe(solution => _solutionSubject.OnNext(solution));
            return _solutionSubject.AsObservable();
        }

        private IDisposable GenerateGenerations(IObserver<IProblemSolution<T, TK>> observer)
        {
            while (CurrentGeneration < NumberOfGenerations)
            {
                if (_state == AlgoritmState.Paused || _state == AlgoritmState.Completed)
                {
                    observer.OnCompleted();
                    return Disposable.Empty;
                }
                GenerateGeneration(observer);
                CurrentGeneration++;
            }

            observer.OnCompleted();
            State = AlgoritmState.Completed;
            return Disposable.Empty;
        }

        private void GenerateGeneration(IObserver<IProblemSolution<T, TK>> observer)
        {
            _population = Selection.CreateNewGeneration(_population, _randomizer);
            Cross();
            Mutation();

            var generationBest = _population.Min(x => x);
            if (generationBest.IsBetterThan(_bestSolution))
            {
                _bestSolution = generationBest;
                observer.OnNext(_bestSolution);
            }

            if (State == AlgoritmState.Paused)
            {
                observer.OnCompleted();
            }
        }

        private void Cross()
        {
            var cross = new List<int>();

            for (int i = 0; i < _population.Count; i++)
                if (_randomizer.NextDouble() < CrossingProbability)
                    cross.Add(i);

            if (cross.Count % 2 == 1)
            {
                int temp;
                do
                    temp = _randomizer.Next(_population.Count);
                while (cross.Contains(temp));
                cross.Add(temp);
            }

            while (cross.Count > 0)
            {
                int firstParent = _randomizer.Next(cross.Count);

                int secondParent;
                do
                    secondParent = _randomizer.Next(cross.Count);
                while (firstParent == secondParent);

                IList<IProblemSolution<T, TK>> descendantes = Crossing.MakeChildren(_population[firstParent], _population[secondParent], _randomizer);
                _population[firstParent] = descendantes[0];
                _population[secondParent] = descendantes[1];

                if (firstParent < secondParent)
                {
                    cross.RemoveAt(secondParent);
                    cross.RemoveAt(firstParent);
                }
                else
                {
                    cross.RemoveAt(firstParent);
                    cross.RemoveAt(secondParent);
                }

            }
        }

        private void Mutation()
        {
            for (int i = 0; i < _population.Count; i++)
            {
                if (_randomizer.NextDouble() >= MutationProbability)
                    continue;
                MutationOperator.Mutate(_population[i], _randomizer);
                i++;
            }
        }

        IObservable<IProblemSolution> IAlgorithm.GetBestSolution(IAlgorithmParams algorithmParams, IProblemInstance instance)
        {
            return GetBestSolution(algorithmParams, (T)instance);
        }

        public IObservable<AlgoritmState> StateStream
        {
            get
            {
                return _stateSubject.AsObservable();
            }
        }
    }
}
