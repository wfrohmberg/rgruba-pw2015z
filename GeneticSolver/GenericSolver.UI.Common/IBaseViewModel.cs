using System;
using System.ComponentModel;

namespace GenericSolver.UI.Common
{
    public interface IBaseViewModel : INotifyPropertyChanged
    {
        IObservable<string> PropertyChangedStream { get; }
    }
}