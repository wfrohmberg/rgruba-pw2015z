﻿using GeneticSolver.Core;

namespace GenericSolver.UI.Common
{
    public interface IAlgorithmParamsViewModel : IBaseViewModel
    {
        bool IsValid { get; }

        IAlgorithmParams AlgorithmParams { get; }

        void UpdateParams(IAlgorithmParams newParams);
    }
}