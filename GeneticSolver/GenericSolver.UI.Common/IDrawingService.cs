﻿using System.Windows.Controls;
using GeneticSolver.Core.Problem;

namespace GenericSolver.UI.Common
{
    public interface IDrawingService<T, TK> where T : IProblemInstance
    {
        void DrawInstance(T instance, Canvas drawingArea);

        void DrawSolution(IProblemSolution<T, TK> solution, Canvas drawingArea);
    }
}