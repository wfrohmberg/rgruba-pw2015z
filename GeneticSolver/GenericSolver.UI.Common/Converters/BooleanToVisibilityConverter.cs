﻿using System;
using System.Windows;
using System.Windows.Data;

namespace GenericSolver.UI.Common.Converters
{
    public class BooleanToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var v = Visibility.Visible;
            if (!(bool)value)
            {
                v = Visibility.Hidden;
            }
            return v;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }
}