﻿using System;
using System.Collections.Generic;
using System.Reactive;
using System.Windows.Controls;
using System.Windows.Input;
using GeneticSolver.Core.Problem;

namespace GenericSolver.UI.Common
{
    public interface IProblemViewModel
    {
        IProblemParamsViewModel ProblemParams { get; }

        ICommand GenerateInstanceCommand { get;  }

        ICommand SolveProblemCommand { get; }

        ICommand StopSolvingCommand { get; }

        IList<IAlgorithmViewModel> Algorithms { get;}

        IAlgorithmViewModel SelectedAlgorithm { get; set; }

        string ProblemName { get; }

        Canvas DrawingArea { get; set; }

        IObservable<bool> IsBusyStream { get; }

        IObservable<bool> CanSaveStream { get; }

        IObservable<bool> CanReadStream { get; }

        string RunAlgorithmLabel { get; set; }

        bool CanStopAlgorithm { get; set; }

        bool CanChangeParameters { get; set; }

        IObservable<Unit> SaveProblemState(string fileName);

        IObservable<IProblemDto> ReadProblemState(string fileName);

        void RestoreProblemState(IProblemDto problemState);
    }
}