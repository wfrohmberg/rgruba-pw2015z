﻿using System;
using System.ComponentModel;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Runtime.CompilerServices;
using GenericSolver.UI.Common.Annotations;

namespace GenericSolver.UI.Common
{
    public abstract class BaseViewModel : IBaseViewModel
    {
        private readonly Subject<string> _subject = new Subject<string>();

        protected BaseViewModel()
        {
            Observable
               .FromEventPattern<PropertyChangedEventHandler, PropertyChangedEventArgs>(
                   h => PropertyChanged += h,
                   h => PropertyChanged -= h)
               .Select(x => x.EventArgs.PropertyName).Subscribe(_subject.OnNext);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public IObservable<string> PropertyChangedStream
        {
            get { return _subject.AsObservable(); }
        }
    }
}