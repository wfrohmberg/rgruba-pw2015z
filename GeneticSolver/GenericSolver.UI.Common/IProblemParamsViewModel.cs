﻿namespace GenericSolver.UI.Common
{
    public interface IProblemParamsViewModel :  IBaseViewModel
    {
        bool IsValid { get; }
    }
}