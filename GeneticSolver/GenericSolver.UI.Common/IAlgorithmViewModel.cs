﻿using System;
using GeneticSolver.Core;
using GeneticSolver.Core.Problem;

namespace GenericSolver.UI.Common
{
    public interface IAlgorithmViewModel : IBaseViewModel
    {
        string AlgorithmName { get; }

        IAlgorithmParamsViewModel AlgorithmParams { get; set; }

        IProblemInstance ProblemInstance { get; set; }
        
        bool IsValid { get; }

        IObservable<IProblemSolution> SolveProblem();

        IAlgorithm Algorithm { get; }

        void PauseSolving();

        IObservable<IProblemSolution> RestartSolving();

        void StopSolving();
    }
}