﻿using Microsoft.Reactive.Testing;
using System.Reactive.Concurrency;

namespace GeneticSolver.Core.Tests
{

    public sealed class ImmediateSchedulers : ISchedulerProvider
    {
        private readonly IScheduler _currentThread = ImmediateScheduler.Instance;
        private readonly IScheduler _dispatcher = ImmediateScheduler.Instance;
        private readonly IScheduler _immediate = ImmediateScheduler.Instance;
        private readonly IScheduler _newThread = ImmediateScheduler.Instance;
        private readonly IScheduler _threadPool = ImmediateScheduler.Instance;

        #region Explicit implementation of ISchedulerService
        IScheduler ISchedulerProvider.CurrentThread { get { return _currentThread; } }
        IScheduler ISchedulerProvider.Dispatcher { get { return _dispatcher; } }
        IScheduler ISchedulerProvider.Immediate { get { return _immediate; } }
        IScheduler ISchedulerProvider.NewThread { get { return _newThread; } }
        IScheduler ISchedulerProvider.ThreadPool { get { return _threadPool; } }
        #endregion
        public IScheduler CurrentThread { get { return _currentThread; } }
        public IScheduler Dispatcher { get { return _dispatcher; } }
        public IScheduler Immediate { get { return _immediate; } }
        public IScheduler NewThread { get { return _newThread; } }
        public IScheduler ThreadPool { get { return _threadPool; } }
    }

}