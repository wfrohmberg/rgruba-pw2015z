﻿using System;
using System.Windows.Controls;
using GeneticSolver.Core.Problem;
using Moq;
using NUnit.Framework;

namespace GeneticSolver.Core.Tests.Problem
{
    [TestFixture]
    public class ProblemBaseTests
    {
        [Test]
        public void ProblemName_Is_Set_On_Construction()
        {
            var sut = new FakeProblem("fake problem", new ImmediateSchedulers());

            Assert.AreEqual("fake problem",sut.ProblemName);
        }

        [Test]
        public void GenerateProblemInstance_OnSubscribe_Calls_GenerateInstance()
        {
            var sut = new FakeProblem("fake problem", new ImmediateSchedulers());
            sut.GenerateInstanceWasCalled = false;
            sut.GenerateProblemInstance(new Mock<IProblemParams>(MockBehavior.Strict).Object).Subscribe();
            Assert.IsTrue(sut.GenerateInstanceWasCalled);
        }
    }

    public class FakeProblem : ProblemBase
    {
        public bool GenerateInstanceWasCalled { get; set; }

        public FakeProblem(string problemName, ISchedulerProvider schedulerProvider)
            : base(problemName, schedulerProvider)
        {
        }

        protected override IProblemInstance GenerateInstance(IProblemParams problemParams)
        {
            GenerateInstanceWasCalled = true;
            return null;
        }

        public override void DrawSolution(IProblemSolution solution, Canvas drawingArea)
        {

        }

        public override void DrawInstance(IProblemInstance instance, Canvas drawingArea)
        {

        }
    }
}