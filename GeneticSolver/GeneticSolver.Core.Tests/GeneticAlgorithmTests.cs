﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reactive.Linq;
using GeneticSolver.Core.Problem;
using Moq;
using NUnit.Framework;

namespace GeneticSolver.Core.Tests
{
    [TestFixture]
    public class GeneticAlgorithmTests
    {
        private Mock<IRandomSoultionGenerator<FakeProblemInstance, int>> _solutionGenerator;

        private Mock<ISelection<FakeProblemInstance, int>> _selection;

        private Mock<ICrossingOperator<FakeProblemInstance, int>> _crossing;

        private Mock<IMutationOperator<FakeProblemInstance, int>> _mutationOperator;

        private Mock<IRandomWrapper> _randomizer;

        private Mock<IGeneticParams<FakeProblemInstance, int>> _parameters;

        private IGeneticAlgorithm<FakeProblemInstance, int> _sut;
        [SetUp]
        public void SetUp()
        {
            _solutionGenerator = new Mock<IRandomSoultionGenerator<FakeProblemInstance, int>>(MockBehavior.Strict)
                .SetupAllProperties();
            _selection = new Mock<ISelection<FakeProblemInstance, int>>(MockBehavior.Strict)
                .SetupAllProperties();
            _crossing = new Mock<ICrossingOperator<FakeProblemInstance, int>>(MockBehavior.Strict)
                .SetupAllProperties();
            _mutationOperator = new Mock<IMutationOperator<FakeProblemInstance, int>>(MockBehavior.Strict)
                .SetupAllProperties();
            _randomizer = new Mock<IRandomWrapper>(MockBehavior.Strict)
                .SetupAllProperties();

            _parameters = new Mock<IGeneticParams<FakeProblemInstance, int>>(MockBehavior.Strict)
                .SetupAllProperties();
            _parameters.SetupGet(x => x.Crossing).Returns(_crossing.Object);
            _parameters.SetupGet(x => x.Selection).Returns(_selection.Object);
            _parameters.SetupGet(x => x.SolutionGenerator).Returns(_solutionGenerator.Object);
            _parameters.SetupGet(x => x.MutationOperator).Returns(_mutationOperator.Object);

            _sut = new GeneticAlgorithm<FakeProblemInstance, int>("algorytm", _randomizer.Object, new ImmediateSchedulers());

        }

        [Test]
        public void AlgorithmName_Set_On_Construction()
        {
            Assert.AreEqual("algorytm", _sut.AlgorithmName);
        }

        [Test]
        public void AlgorithmState_Is_Ininitialized_On_Construction()
        {
            Assert.AreEqual(AlgoritmState.Initialized, _sut.State);
        }

        [Test]
        public void StateStream_Produce_Initialize_By_Default()
        {
            var state = AlgoritmState.Started;
            _sut.StateStream.Take(1).Subscribe(s => state = s);
            Assert.AreEqual(AlgoritmState.Initialized, _sut.State);
        }

        [Test]
        public void GetBestSolution_Set_Algorithm_To_State()
        {
            var state = AlgoritmState.Initialized;
            _sut.StateStream.Skip(1).Take(1).Subscribe(s => state = s);

            _sut.GetBestSolution(_parameters.Object, new FakeProblemInstance());

            Assert.AreEqual(AlgoritmState.Started, state);
        }

        [Test]
        public void GetBestSolution_Sets_Parameters()
        {
            _solutionGenerator.Setup(x => x.Generate(It.IsAny<FakeProblemInstance>(), It.IsAny<IRandomWrapper>()))
                .Returns(new Mock<IProblemSolution<FakeProblemInstance, int>>().Object);

            _selection.Setup(x => x.CreateNewGeneration(It.IsAny<IList<IProblemSolution<FakeProblemInstance, int>>>(), It.IsAny<IRandomWrapper>()))
                .Returns(
                    new List<IProblemSolution<FakeProblemInstance, int>>
                    {
                        BuildSolution(1)
                    });
            _randomizer.Setup(x => x.NextDouble()).Returns(1000);
            _parameters.SetupGet(x => x.MutationProbability).Returns(100);
            _parameters.SetupGet(x => x.NumberOfGenerations).Returns(200);
            _parameters.SetupGet(x => x.SizeOfPopulation).Returns(1000);
            _parameters.SetupGet(x => x.CrossingProbability).Returns(50);

            _sut.GetBestSolution(_parameters.Object, new FakeProblemInstance());

            Assert.AreEqual(100, _sut.MutationProbability);
            Assert.AreEqual(200, _sut.NumberOfGenerations);
            Assert.AreEqual(1000, _sut.SizeOfPopulation);
            Assert.AreEqual(50, _sut.CrossingProbability);
            Assert.AreSame(_crossing.Object, _sut.Crossing);
            Assert.AreSame(_mutationOperator.Object, _sut.MutationOperator);
            Assert.AreSame(_solutionGenerator.Object, _sut.SolutionGenerator);
            Assert.AreSame(_selection.Object, _sut.Selection);
        }

        [Test]
        public void GetBestSolution_Delegates_To_Solution_Generation_To_Generate_First_Population()
        {
            var solutionItem = new Mock<IProblemSolution<FakeProblemInstance, int>>();
            _solutionGenerator.Setup(x => x.Generate(It.IsAny<FakeProblemInstance>(), It.IsAny<IRandomWrapper>()))
                .Returns(solutionItem.Object);

            _selection.Setup(x => x.CreateNewGeneration(It.IsAny<IList<IProblemSolution<FakeProblemInstance, int>>>(), It.IsAny<IRandomWrapper>()))
                .Returns(
                    new List<IProblemSolution<FakeProblemInstance, int>>
                    {
                        BuildSolution(1)
                    });
            int sizeOfPopulation = 1000;
            _randomizer.Setup(x => x.NextDouble()).Returns(1000);
            _parameters.SetupGet(x => x.MutationProbability).Returns(100);
            _parameters.SetupGet(x => x.NumberOfGenerations).Returns(200);
            _parameters.SetupGet(x => x.SizeOfPopulation).Returns(sizeOfPopulation);
            _parameters.SetupGet(x => x.CrossingProbability).Returns(50);

            _sut.GetBestSolution(_parameters.Object, new FakeProblemInstance());

            _solutionGenerator.Verify(x => x.Generate(It.IsAny<FakeProblemInstance>(), It.IsAny<IRandomWrapper>()), Times.Exactly(sizeOfPopulation));
        }

        IProblemSolution<FakeProblemInstance, int> BuildSolution(int solutionValue)
        {
            var solution = new Mock<IProblemSolution<FakeProblemInstance, int>>();
            solution.SetupGet(x => x.SolutionValue).Returns(solutionValue);

            return solution.Object;
        }
    }

    public class FakeProblemInstance : IProblemInstance
    {

    }
}