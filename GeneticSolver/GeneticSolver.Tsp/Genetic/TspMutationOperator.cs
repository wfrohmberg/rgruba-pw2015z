﻿using GeneticSolver.Core;
using GeneticSolver.Core.Problem;
using GeneticSolver.Tsp.Problem;

namespace GeneticSolver.Tsp.Genetic
{
    public class TspMutationOperator : IMutationOperator<TspGraph, BasePath>
    {
        public void Mutate(IProblemSolution<TspGraph, BasePath> problemSolution, IRandomWrapper randomizer)
        {
            int p1 = randomizer.Next(problemSolution.SolutionValue.Vertexes.Count);
            int p2 = (p1 + 1) % problemSolution.SolutionValue.Vertexes.Count;
            int temp = problemSolution.SolutionValue.Vertexes[p1];

            problemSolution.SolutionValue.Vertexes[p1] = problemSolution.SolutionValue.Vertexes[p2];
            problemSolution.SolutionValue.Vertexes[p2] = temp;

            problemSolution.SolutionValue.Evaluate();
        }
    }
}