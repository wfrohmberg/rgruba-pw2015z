﻿using System.Collections.Generic;
using System.Linq;
using GeneticSolver.Core;
using GeneticSolver.Core.Problem;
using GeneticSolver.Tsp.Problem;

namespace GeneticSolver.Tsp.Genetic.Selection
{
    public class RouletteSelection : ISelection<TspGraph, BasePath>
    {
        public IList<IProblemSolution<TspGraph, BasePath>> CreateNewGeneration(IList<IProblemSolution<TspGraph, BasePath>> population, IRandomWrapper randomizer)
        {
            var max = population.Max(x => x.SolutionValue.Distance);

            var probabilities = new int[population.Count];
            var sum = 0;

            for (int i = 0; i < population.Count; i++)
            {
                sum += max - population[i].SolutionValue.Distance;
                probabilities[i] = sum;
            }

            var newGeneration = new List<IProblemSolution<TspGraph, BasePath>>();

            for (int i = 0; i < population.Count; i++)
            {
                int temp = randomizer.Next(0, sum);
                int j;
                for (j = 0; j < population.Count - 1; j++)
                {

                    if (probabilities[j] > temp)
                        break;
                }
                newGeneration.Add(population[j]);
            }
            return newGeneration;
        }

        public string Name
        {
            get { return "Ruletka"; }
        }
    }
}