﻿using System.Collections.Generic;
using System.Linq;
using GeneticSolver.Core;
using GeneticSolver.Core.Problem;
using GeneticSolver.Tsp.Problem;

namespace GeneticSolver.Tsp.Genetic.Selection
{
    public class LinearRankingSelection : ISelection<TspGraph, BasePath>
    {
        public IList<IProblemSolution<TspGraph, BasePath>> CreateNewGeneration(IList<IProblemSolution<TspGraph, BasePath>> population, IRandomWrapper randomizer)
        {
            var oldPopulation = population.OrderByDescending(x => x).ToList();
            var probabilities = new int[population.Count];

            var sum = 0;
            for (int i = 0; i < population.Count; i++)
            {
                sum += i + 1;
                probabilities[i] = sum;


            }
            var newGeneration = new List<IProblemSolution<TspGraph, BasePath>>();
            int j;
            for (int i = 0; i < population.Count; i++)
            {
                int temp = randomizer.Next(0, sum);
                for (j = 0; j < population.Count - 1; j++)
                {

                    if (probabilities[j] > temp)
                        break;
                }
                newGeneration.Add(oldPopulation[j]);
            }
            return newGeneration;
        }

        public string Name
        {
            get { return "Ranking"; }
        }
    }
}