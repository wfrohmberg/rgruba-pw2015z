﻿using System;
using System.Collections.Generic;
using GeneticSolver.Core;
using GeneticSolver.Core.Problem;
using GeneticSolver.Tsp.Problem;

namespace GeneticSolver.Tsp.Genetic.CrossingOperators
{
    public class CxCrossingOperator : ICrossingOperator<TspGraph, BasePath>
    {
        public IList<IProblemSolution<TspGraph, BasePath>> MakeChildren(IProblemSolution<TspGraph, BasePath> father, IProblemSolution<TspGraph, BasePath> mother, IRandomWrapper randomizer)
        {
            var childs = new List<IProblemSolution<TspGraph, BasePath>>(2)
            {
                MakeChild(mother, father),
                MakeChild(father, mother)
            };

            return childs;
        }

        public string Name
        {
            get
            {
                return "Cyclic Crossover";
            }
        }

        private IProblemSolution<TspGraph, BasePath> MakeChild(IProblemSolution<TspGraph, BasePath> parentOne, IProblemSolution<TspGraph, BasePath> parentTwo)
        {
            BasePath child = new EmptyPath(parentOne.Instance);
            var solution = new TspSolution(parentOne.Instance, child);

            int index = 0, vertex;
            do
            {
                child.Vertexes[index] = parentTwo.SolutionValue.Vertexes[index];
                vertex = parentOne.SolutionValue.Vertexes[index];
                index = parentTwo.SolutionValue.Vertexes.IndexOf(vertex);
            } while (child.Vertexes.IndexOf(vertex) == -1);

            for (int i = 0; i < parentOne.SolutionValue.Vertexes.Count; i++)
            {
                if (child.Vertexes[i] == 0)
                    child.Vertexes[i] = parentOne.SolutionValue.Vertexes[i];
            }
            solution.Evaluate();
            return solution;
        }
    }
}