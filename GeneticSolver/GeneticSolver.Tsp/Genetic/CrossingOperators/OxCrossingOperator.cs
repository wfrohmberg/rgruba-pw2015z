﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GeneticSolver.Core;
using GeneticSolver.Core.Problem;
using GeneticSolver.Tsp.Problem;

namespace GeneticSolver.Tsp.Genetic.CrossingOperators
{
    public class OxCrossingOperator : ICrossingOperator<TspGraph, BasePath>
    {
        public IList<IProblemSolution<TspGraph, BasePath>> MakeChildren(IProblemSolution<TspGraph, BasePath> father, IProblemSolution<TspGraph, BasePath> mother, IRandomWrapper randomizer)
        {
            var childs = new List<IProblemSolution<TspGraph, BasePath>>(2);
            var fatherCopy = new Path(father.Instance)
            {
                Vertexes = father.SolutionValue.Vertexes.Select(x => x).ToList()
            };

            var motherCopy = new Path(mother.Instance)
            {
                Vertexes = mother.SolutionValue.Vertexes.Select(x => x).ToList()
            };

            childs.Add(new TspSolution(father.Instance, fatherCopy));
           childs.Add(new TspSolution(mother.Instance, motherCopy));

            int cut1 = randomizer.Next(father.Instance.Points.Length + 1), cut2;
            do
                cut2 = randomizer.Next(father.Instance.Points.Length + 1);
            while (cut1 == cut2);

            if (cut1 > cut2)
            {
                int temp = cut1;
                cut1 = cut2;
                cut2 = temp;
            }
            var firstChildUsed = new List<int>();
            var secondChildUsed = new List<int>();
            var chain0 = new List<int>();
            var chain1 = new List<int>();
            for (int i = cut1; i < cut2; i++)
            {
                firstChildUsed.Add(childs[0].SolutionValue.Vertexes[i] = father.SolutionValue.Vertexes[i]);
                secondChildUsed.Add(childs[1].SolutionValue.Vertexes[i] = mother.SolutionValue.Vertexes[i]);
            }
            for (int i = 0; i < father.SolutionValue.Vertexes.Count; i++)
            {
                int index = (i + cut2) % father.SolutionValue.Vertexes.Count;
                if (!secondChildUsed.Contains(father.SolutionValue.Vertexes[index]))
                    chain0.Add(father.SolutionValue.Vertexes[index]);
                if (!firstChildUsed.Contains(mother.SolutionValue.Vertexes[index]))
                    chain1.Add(mother.SolutionValue.Vertexes[index]);
            }
            if (cut2 - cut1 < father.SolutionValue.Vertexes.Count)
                for (int i = cut2, index = 0; i != cut1 && index < chain1.Count; i++, index++)
                {
                    if (i >= father.SolutionValue.Vertexes.Count)
                        i = 0;
                    childs[0].SolutionValue.Vertexes[i] = chain1[index];
                    childs[1].SolutionValue.Vertexes[i] = chain0[index];
                }


            return childs;
        }

        public string Name
        {
            get
            {
                return "Ordered Crossover";
            }
        }
    }
}