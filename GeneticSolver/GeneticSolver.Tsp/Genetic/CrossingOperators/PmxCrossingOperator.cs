﻿using System.Collections.Generic;
using System.Linq;
using GeneticSolver.Core;
using GeneticSolver.Core.Problem;
using GeneticSolver.Tsp.Problem;

namespace GeneticSolver.Tsp.Genetic.CrossingOperators
{
    public class PmxCrossingOperator : ICrossingOperator<TspGraph, BasePath>
    {
        public IList<IProblemSolution<TspGraph, BasePath>> MakeChildren(IProblemSolution<TspGraph, BasePath> father, IProblemSolution<TspGraph, BasePath> mother, IRandomWrapper randomizer)
        {
            var childs = new List<IProblemSolution<TspGraph, BasePath>>(2);
            var fatherCopy = new Path(father.Instance)
            {
                Vertexes = father.SolutionValue.Vertexes.Select(x => x).ToList()
            };

            var motherCopy = new Path(mother.Instance)
            {
                Vertexes = mother.SolutionValue.Vertexes.Select(x => x).ToList()
            };

            childs.Add(new TspSolution(father.Instance, fatherCopy));
           childs.Add(new TspSolution(mother.Instance, motherCopy));


           int cut1 = randomizer.Next(father.SolutionValue.Vertexes.Count - 1), cut2;
            do
                cut2 = randomizer.Next(father.SolutionValue.Vertexes.Count - 1);
            while (cut1 == cut2);

            if (cut1 > cut2)
            {
                int temp = cut1;
                cut1 = cut2;
                cut2 = temp;
            }

            var firstChildUsed = new List<int>();
            var secondChildUsed = new List<int>();
            for (int i = cut1; i < cut2; i++)
            {
                secondChildUsed.Add(childs[0].SolutionValue.Vertexes[i] = mother.SolutionValue.Vertexes[i]);
                firstChildUsed.Add(childs[1].SolutionValue.Vertexes[i] = father.SolutionValue.Vertexes[i]);
            }

            for (int i = 0; i < father.SolutionValue.Vertexes.Count; i++)
            {
                if (i >= cut1 && i < cut2)
                    continue;

                while (secondChildUsed.Contains(childs[0].SolutionValue.Vertexes[i]))
                    childs[0].SolutionValue.Vertexes[i] = firstChildUsed[secondChildUsed.IndexOf(childs[0].SolutionValue.Vertexes[i])];

                while (firstChildUsed.Contains(childs[1].SolutionValue.Vertexes[i]))
                    childs[1].SolutionValue.Vertexes[i] = secondChildUsed[firstChildUsed.IndexOf(childs[1].SolutionValue.Vertexes[i])];
            }

            // var t1 = result[0].Data.

            return childs;
        }

        public string Name
        {
            get
            {
                return "Partially Matched Crossover";
            }
        }
    }
}