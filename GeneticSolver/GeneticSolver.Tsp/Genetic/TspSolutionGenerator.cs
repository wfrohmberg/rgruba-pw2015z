﻿using GeneticSolver.Core;
using GeneticSolver.Core.Problem;
using GeneticSolver.Tsp.Problem;

namespace GeneticSolver.Tsp.Genetic
{
    public class TspSolutionGenerator : IRandomSoultionGenerator<TspGraph, BasePath>
    {
        public IProblemSolution<TspGraph, BasePath> Generate(TspGraph instance, IRandomWrapper randomizer)
        {
            var path = new Path(instance);
            var solution = new TspSolution(instance, path);

            for (int i = 0; i < instance.Points.Length; i++)
            {
                path.Vertexes.Add(-1);
            }
            for (int i = 0; i < instance.Points.Length; i++)
            {
                int tmp;
                do
                    tmp = randomizer.Next(instance.Points.Length);
                while (path.Vertexes.Contains(tmp));
                path.Vertexes[i] = tmp;
            }

            return solution;
        }

        public IProblemSolution<TspGraph, BasePath> RestoreSolution(TspGraph instance, BasePath result)
        {
            var path = new Path(instance);
            var solution = new TspSolution(instance, path);

            for (var i = 0; i < result.Vertexes.Count; i++)
            {
                path.Vertexes.Add(result.Vertexes[i]);
            }

            return solution;
        } 
    }
}