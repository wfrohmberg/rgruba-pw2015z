﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using GenericSolver.UI.Common;
using GeneticSolver.Core;
using GeneticSolver.Core.Extenstions;
using GeneticSolver.Tsp.Problem;

namespace GeneticSolver.Tsp.ViewModels
{
    public class TspGeneticAlgorithmParamsViewModel : BaseViewModel, ITspGeneticAlgorithmParamsViewModel, IDataErrorInfo
    {
        private readonly IDictionary<string, string> _errors = new Dictionary<string, string>();

        public TspGeneticAlgorithmParamsViewModel(IGeneticParams<TspGraph, BasePath> algorithmParams)
        {
            AlgorithmParams = algorithmParams;
            Selections = new ObservableCollection<ISelection<TspGraph, BasePath>>(
                    Assembly.GetExecutingAssembly().CreateInstancesOfTypesWhichImplements<ISelection<TspGraph, BasePath>>());

            CrossingOperators = new ObservableCollection<ICrossingOperator<TspGraph, BasePath>>(
                 Assembly.GetExecutingAssembly().CreateInstancesOfTypesWhichImplements<ICrossingOperator<TspGraph, BasePath>>());
        }

        public IList<ISelection<TspGraph, BasePath>> Selections { get; private set; }

        public ISelection<TspGraph, BasePath> Selection
        {
            get { return AlgorithmParams.Selection; }
            set
            {
                if (Equals(value, AlgorithmParams.Selection)) return;
                AlgorithmParams.Selection = value;
                OnPropertyChanged();
            }
        }

        public IList<ICrossingOperator<TspGraph, BasePath>> CrossingOperators { get; private set; }

        public ICrossingOperator<TspGraph, BasePath> Crossing
        {
            get { return AlgorithmParams.Crossing; }
            set
            {
                if (Equals(value, AlgorithmParams.Crossing)) return;
                AlgorithmParams.Crossing = value;
                OnPropertyChanged();
            }
        }

        public int SizeOfPopulation
        {
            get { return AlgorithmParams.SizeOfPopulation; }
            set
            {
                if (value == AlgorithmParams.SizeOfPopulation) return;
                AlgorithmParams.SizeOfPopulation = value;
                OnPropertyChanged();
            }
        }

        public int NumberOfGenerations
        {
            get { return AlgorithmParams.NumberOfGenerations; }
            set
            {
                if (value == AlgorithmParams.NumberOfGenerations) return;
                AlgorithmParams.NumberOfGenerations = value;
                OnPropertyChanged();
            }
        }

        public double CrossingProbability
        {
            get { return AlgorithmParams.CrossingProbability; }
            set
            {
                AlgorithmParams.CrossingProbability = value;
                OnPropertyChanged();
            }
        }

        public double MutationProbability
        {
            get { return AlgorithmParams.MutationProbability; }
            set
            {
                AlgorithmParams.MutationProbability = value;
                OnPropertyChanged();
            }
        }

        private IGeneticParams<TspGraph, BasePath> _algorithmParams;

        public IGeneticParams<TspGraph, BasePath> AlgorithmParams
        {
            get { return _algorithmParams; }
            private set
            {
                if (Equals(value, _algorithmParams)) return;
                _algorithmParams = value;
            }
        }

        public string this[string columnName]
        {
            get
            {
                string result = null;
                switch (columnName)
                {
                    case "Selection":
                        if (Selection == null)
                            result = "Metoda selekcji nie może być pusta";
                        break;
                    case "Crossing":
                        if (Selection == null)
                            result = "Operator krzyżowania nie może być pusty";
                        break;
                    case "SizeOfPopulation":
                        if (SizeOfPopulation <= 10)
                            result = "Rozmiar populacji musi być większy od 10";
                        break;
                    case "NumberOfGenerations":
                        if (NumberOfGenerations < 1)
                            result = "Liczba generacji musi być większa od 0";
                        break;
                    case "CrossingProbability":
                        if (CrossingProbability < 0 || CrossingProbability > 1)
                            result = "Prawdopodobieństwo musi być w przedziale <0,1>";
                        break;
                    case "MutationProbability":
                        if (MutationProbability < 0 || MutationProbability > 1)
                            result = "Prawdopodobieństwo musi być w przedziale <0,1>";
                        break;
                }
                _errors[columnName] = result;
                OnPropertyChanged("IsValid");
                return result;

            }
        }

        public string Error { get { return string.Join(",", _errors.Values.Where(x => x != null)); } }

        public bool IsValid
        {
            get { return _errors.Values.All(x => x == null); }
        }

        IAlgorithmParams IAlgorithmParamsViewModel.AlgorithmParams
        {
            get { return AlgorithmParams; }
        }

        public void UpdateParams(IAlgorithmParams newParams)
        {
            var par = (IGeneticParams<TspGraph, BasePath>) newParams;
            MutationProbability = par.MutationProbability;
            CrossingProbability = par.CrossingProbability;
            NumberOfGenerations = par.NumberOfGenerations;
            SizeOfPopulation = par.SizeOfPopulation;

            if (par.Crossing != null)
                Crossing = CrossingOperators.FirstOrDefault(c => c.Name.Equals(par.Crossing.Name));
            if (par.Selection != null)
                Selection = Selections.FirstOrDefault(c => c.Name.Equals(par.Selection.Name));
        }
    }
}