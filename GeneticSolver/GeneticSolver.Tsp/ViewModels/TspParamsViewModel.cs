﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using GenericSolver.UI.Common;
using GeneticSolver.Tsp.Problem;

namespace GeneticSolver.Tsp.ViewModels
{   
    public class TspParamsViewModel : BaseViewModel, ITspParamsViewModel, IDataErrorInfo
    {
        private readonly ITspProblemParams _params;
        private readonly IDictionary<string, string> _errors = new Dictionary<string, string>();

        public TspParamsViewModel(ITspProblemParams @params)
        {
            _params = @params;
        }

        public int NumberOfElements
        {
            get { return _params.NumberOfElements; }
            set
            {
                if (value == _params.NumberOfElements) return;
                _params.NumberOfElements = value;
                OnPropertyChanged();
            }
        }

        public string this[string columnName]
        {
            get
            {
                string result = null;
                switch (columnName)
                {
                    case "NumberOfElements":
                        if (NumberOfElements < 3)
                        {
                            result = "Liczba miast  musi być większa od 2";

                        }
                        _errors["NumberOfElements"] = result;
                        break;
                }
                OnPropertyChanged("IsValid");
                return result;

            }
        }

        public string Error { get { return string.Join(",", _errors.Values.Where(x => x != null)); } }

        public bool IsValid
        {
            get { return _errors.Values.All(x => x == null); }
        }
    }
}