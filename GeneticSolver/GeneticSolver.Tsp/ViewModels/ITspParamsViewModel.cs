﻿using GenericSolver.UI.Common;

namespace GeneticSolver.Tsp.ViewModels
{
    public interface ITspParamsViewModel : IProblemParamsViewModel
    {
        int NumberOfElements { get; set; }
    }
}