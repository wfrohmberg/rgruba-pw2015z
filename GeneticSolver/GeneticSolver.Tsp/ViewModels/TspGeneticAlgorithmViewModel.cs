﻿using System;
using System.ComponentModel;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using Caliburn.Micro;
using GenericSolver.UI.Common;
using GeneticSolver.Core;
using GeneticSolver.Core.Problem;
using GeneticSolver.Tsp.Genetic;
using GeneticSolver.Tsp.Problem;

namespace GeneticSolver.Tsp.ViewModels
{
    public class TspGeneticAlgorithmViewModel : Conductor<object>, IAlgorithmViewModel
    {
        private readonly IAlgorithm<TspGraph, BasePath> _algorithm;
        private readonly IGeneticParams<TspGraph, BasePath> _geneticParams = new GeneticParams<TspGraph, BasePath>();
        private readonly SerialDisposable _isValidSubscription = new SerialDisposable();

        public TspGeneticAlgorithmViewModel()
        {
            _geneticParams.MutationOperator = new TspMutationOperator();
            _geneticParams.SolutionGenerator = new TspSolutionGenerator();

            _algorithm = new GeneticAlgorithm<TspGraph, BasePath>("Algorytm Genetyczny");
            AlgorithmParams = new TspGeneticAlgorithmParamsViewModel(_geneticParams);
        }

        public string AlgorithmName
        {
            get { return _algorithm.AlgorithmName; }
        }

        private IAlgorithmParamsViewModel _algorithmParams;

        public IAlgorithmParamsViewModel AlgorithmParams
        {
            get { return _algorithmParams; }
            set
            {
                if (Equals(value, _algorithmParams)) return;
                _algorithmParams = value;
                NotifyOfPropertyChange(() => AlgorithmParams);
                _isValidSubscription.Disposable = AlgorithmParams.PropertyChangedStream.Where(p => p.Equals("IsValid"))
                .Subscribe(_ => NotifyOfPropertyChange(() => IsValid));

                ActivateItem(AlgorithmParams);
            }
        }

        private IProblemInstance _problemInstance;

        public IProblemInstance ProblemInstance
        {
            get { return _problemInstance; }
            set
            {
                if (Equals(value, _problemInstance)) return;
                _problemInstance = value;
                NotifyOfPropertyChange(() => ProblemInstance);
                NotifyOfPropertyChange(() => IsValid);
            }
        }

        public bool IsValid
        {
            get
            {
                return AlgorithmParams.IsValid && ProblemInstance != null;
            }
        }

        public IObservable<IProblemSolution> SolveProblem()
        {
            return _algorithm.GetBestSolution(_geneticParams, _problemInstance);
        }

        public IAlgorithm Algorithm
        {
            get { return _algorithm; }
        }

        public void PauseSolving()
        {
            _algorithm.PauseSolving();
        }

        public IObservable<IProblemSolution> RestartSolving()
        {
            return _algorithm.RestartSolving();
        }

        public void StopSolving()
        {
            _algorithm.StopSolving();
        }

        public IObservable<string> PropertyChangedStream
        {
            get
            {
                return Observable.FromEventPattern<PropertyChangedEventHandler, PropertyChangedEventArgs>(
                            h => PropertyChanged += h,
                            h => PropertyChanged -= h)
                        .Select(x => x.EventArgs.PropertyName);
            }

        }
    }
}