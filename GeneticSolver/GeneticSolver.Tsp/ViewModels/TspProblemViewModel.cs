﻿

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reactive.Concurrency;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Reflection;
using System.Windows.Controls;
using System.Windows.Input;
using Caliburn.Micro;
using GenericSolver.UI.Common;
using GeneticSolver.Core;
using GeneticSolver.Core.Extenstions;
using GeneticSolver.Core.Problem;
using GeneticSolver.Tsp.Problem;

namespace GeneticSolver.Tsp.ViewModels
{
    public class TspProblemViewModel : Conductor<object>, IProblemViewModel
    {
        private TspProblem _problem = new TspProblem();

        private readonly ISubject<bool> _isBusySubject = new Subject<bool>();
        private readonly SerialDisposable _isValidSubscription = new SerialDisposable();
        private readonly SerialDisposable _isValidProblemParamsSubscription = new SerialDisposable();
        private readonly SerialDisposable _algoritmStateSubscription = new SerialDisposable();
        private readonly SerialDisposable _solutionSubscription = new SerialDisposable();
        private readonly IProblemSerializer<TspProblem> _problemSerializer = new TspSerializer();
        private readonly IProblemDeserializer<TspProblem> _problemDeserializer = new TspSerializer();

        private Action<object> _executeAction;
        private Predicate<object> _canExecutePredicate;
        public TspProblemViewModel()
        {
            ProblemParams = new TspParamsViewModel(_problem.ProblemParams);
            CanChangeParameters = true;
            _executeAction = _ => SolveProblem();
            _canExecutePredicate = _ => CanSolveProblem();
            Algorithms = new ObservableCollection<IAlgorithmViewModel>(
                    Assembly.GetExecutingAssembly()
                    .CreateInstancesOfTypesWhichImplements<IAlgorithmViewModel>());
            GenerateInstanceCommand = new DelegateCommand(_ => GenerateInstance(), _ => CanGenerateInstance());
            SolveProblemCommand = new DelegateCommand(ExecuteAction, CanExecuteAction);
            StopSolvingCommand = new DelegateCommand(_ => StopSolvingProblem(), _ => CanStopAlgorithm);

        }

        private bool CanExecuteAction(object obj)
        {
            return _canExecutePredicate(obj);
        }

        private void ExecuteAction(object obj)
        {
            _executeAction(obj);
        }

        private void PauseSolving()
        {
            _isBusySubject.OnNext(false);
            SelectedAlgorithm.PauseSolving();
        }

        private void RestartSolving()
        {
            _isBusySubject.OnNext(true);
          
            _solutionSubscription.Disposable =   SelectedAlgorithm.RestartSolving()
                .ObserveOn(DispatcherScheduler.Current)
                .SubscribeOn(ThreadPoolScheduler.Instance)
                .Subscribe(OnNewSolution, () => _isBusySubject.OnNext(false));
        }

        private void OnNewSolution(IProblemSolution solution)
        {
            SolutionResult = ((BasePath) solution.SolutionValue).Distance;
            _problem.CurrentSolution = solution;
            _problem.DrawSolution(solution, DrawingArea);
        }

        private void StopSolvingProblem()
        {
            SelectedAlgorithm.StopSolving();
            _isBusySubject.OnNext(false);
        }

        private bool CanSolveProblem()
        {
            return SelectedAlgorithm != null && SelectedAlgorithm.IsValid;
        }

        private void SolveProblem()
        {
            _isBusySubject.OnNext(true);
            _solutionSubscription.Disposable = SelectedAlgorithm.SolveProblem()
                .ObserveOn(DispatcherScheduler.Current)
                .SubscribeOn(ThreadPoolScheduler.Instance)
                .Subscribe(OnNewSolution, () => _isBusySubject.OnNext(false));
        }

        private int? _solutionResult;

        public int? SolutionResult
        {
            get { return _solutionResult; }
            set
            {
                if (value == _solutionResult) return;
                _solutionResult = value;
                NotifyOfPropertyChange(() => SolutionResult);
            }
        }

        private bool CanGenerateInstance()
        {
            return ProblemParams.IsValid;
        }

        private void GenerateInstance()
        {
            _isBusySubject.OnNext(true);
            _problem.GenerateProblemInstance(_problem.ProblemParams)
                .SubscribeOn(ThreadPoolScheduler.Instance)
                .ObserveOn(DispatcherScheduler.Current)
                .Subscribe(problemInstance =>
                {
                    UpdateProblemInstante(problemInstance);
                    _isBusySubject.OnNext(false);
                });
        }

        private void UpdateProblemInstante(IProblemInstance problemInstance)
        {
            Algorithms.ForEach(a => a.ProblemInstance = problemInstance);
            _problem.ProblemInstance = problemInstance;
            _problem.DrawInstance(problemInstance, DrawingArea);
            _problem.CurrentSolution = null;
        }

        private IProblemParamsViewModel _problemParams;

        public IProblemParamsViewModel ProblemParams
        {
            get { return _problemParams; }
            private set
            {
                if (Equals(value, _problemParams)) return;
                _problemParams = value;
                NotifyOfPropertyChange(() => ProblemParams);
                _isValidProblemParamsSubscription.Disposable = ProblemParams.PropertyChangedStream
               .Where(p => p.Equals("IsValid"))
               .Subscribe(_ => ((DelegateCommand)GenerateInstanceCommand).RaiseCanExecuteChanged());
            }
        }

        public ICommand GenerateInstanceCommand { get; private set; }

        public ICommand SolveProblemCommand { get; private set; }

        public ICommand StopSolvingCommand { get; private set; }

        private IList<IAlgorithmViewModel> _algorithms;

        public IList<IAlgorithmViewModel> Algorithms
        {
            get { return _algorithms; }
            private set
            {
                if (Equals(value, _algorithms)) return;
                _algorithms = value;
                NotifyOfPropertyChange(() => Algorithms);
            }
        }

        private IAlgorithmViewModel _selectedAlgorithm;

        public IAlgorithmViewModel SelectedAlgorithm
        {
            get { return _selectedAlgorithm; }
            set
            {
                if (Equals(value, _selectedAlgorithm)) return;
                _selectedAlgorithm = value;
                SubscribeToStateStreamAlgoritm();
                SubscribeToIsValidOnAlgorithm();
                NotifyOfPropertyChange(() => SelectedAlgorithm);
                ActivateItem(value);
            }
        }

        private void SubscribeToIsValidOnAlgorithm()
        {
            _isValidSubscription.Disposable = SelectedAlgorithm.PropertyChangedStream.ObserveOn(DispatcherScheduler.Current)
                .Where(p => p.Equals("IsValid"))
                .Subscribe(_ => ((DelegateCommand)SolveProblemCommand).RaiseCanExecuteChanged());
        }

        private void SubscribeToStateStreamAlgoritm()
        {
            if (SelectedAlgorithm == null)
                return;

            _algoritmStateSubscription.Disposable = SelectedAlgorithm.Algorithm.StateStream.ObserveOn(DispatcherScheduler.Current)
                .Subscribe(state =>
                {
                    switch (state)
                    {
                        case AlgoritmState.Initialized:
                        case AlgoritmState.Completed:
                            _executeAction = _ => SolveProblem();
                            _canExecutePredicate = _ => CanSolveProblem();
                            RunAlgorithmLabel = "Start";
                            CanStopAlgorithm = false;
                            CanChangeParameters = true;
                            _isBusySubject.OnNext(false);
                            break;
                        case AlgoritmState.Paused:
                            _executeAction = _ => RestartSolving();
                            _canExecutePredicate = _ => true;
                            RunAlgorithmLabel = "Continue";
                            CanStopAlgorithm = true;
                            break;
                        case AlgoritmState.Started:
                            _executeAction = _ => PauseSolving();
                            _canExecutePredicate = _ => true;
                            RunAlgorithmLabel = "Pause";
                            CanStopAlgorithm = true;
                            CanChangeParameters = false;
                            break;
                    }
                    ((DelegateCommand)StopSolvingCommand).RaiseCanExecuteChanged();
                });
        }

        public string ProblemName
        {
            get { return _problem.ProblemName; }
        }

        private Canvas _drawingArea;

        public Canvas DrawingArea
        {
            get { return _drawingArea; }
            set
            {
                _drawingArea = value;
                _problem.ProblemParams.Right = (int)_drawingArea.ActualWidth;
                _problem.ProblemParams.Bottom = (int)_drawingArea.ActualHeight;
            }
        }

        public IObservable<bool> IsBusyStream
        {
            get { return _isBusySubject.AsObservable(); }
        }

        public IObservable<bool> CanSaveStream
        {
            get { return Observable.Start(() => false).Merge(IsBusyStream.Select(v => !v)); }
        }

        public IObservable<bool> CanReadStream
        {
            get { return Observable.Start(() => true).Merge(IsBusyStream.Select(v => !v)); }
        }

        private string _runAlgorithmLabel = "Start";

        public string RunAlgorithmLabel
        {
            get { return _runAlgorithmLabel; }
            set
            {
                if (value == _runAlgorithmLabel) return;
                _runAlgorithmLabel = value;
                NotifyOfPropertyChange(() => RunAlgorithmLabel);
            }
        }

        private bool _canStopAlgorithm;

        public bool CanStopAlgorithm
        {
            get { return _canStopAlgorithm; }
            set
            {
                if (value.Equals(_canStopAlgorithm)) return;
                _canStopAlgorithm = value;
                NotifyOfPropertyChange(() => CanStopAlgorithm);
            }
        }

        private bool _canChangeParameters;


        public bool CanChangeParameters
        {
            get { return _canChangeParameters; }
            set
            {
                if (value.Equals(_canChangeParameters)) return;
                _canChangeParameters = value;
                NotifyOfPropertyChange(() => CanChangeParameters);
            }
        }

        public IObservable<Unit> SaveProblemState(string fileName)
        {
            return Observable.Create<Unit>(observer =>
            {

                string serializedObject = _problemSerializer.Serialize(_problem,
                    SelectedAlgorithm != null ? SelectedAlgorithm.Algorithm : null,
                    SelectedAlgorithm != null ? SelectedAlgorithm.AlgorithmParams.AlgorithmParams : null);
                try
                {
                    if (File.Exists(fileName))
                        File.Delete(fileName);
                    using (TextWriter textWriter = File.CreateText(fileName))
                    {
                        textWriter.Write(serializedObject);
                        textWriter.Flush();
                    }
                }
                catch (Exception e)
                {
                    observer.OnError(e);
                }
                observer.OnNext(Unit.Default);
                observer.OnCompleted();
                return Disposable.Empty;
            });
        }

        public IObservable<IProblemDto> ReadProblemState(string fileName)
        {
            return Observable.Create<IProblemDto>(observer =>
            {
                try
                {
                    if (!File.Exists(fileName))
                        throw new FileNotFoundException(fileName);

                    observer.OnNext(_problemDeserializer.Deserialize(File.ReadAllText(fileName)));

                }
                catch (Exception e)
                {
                    observer.OnError(e);
                }

                observer.OnCompleted();
                return Disposable.Empty;
            });
        }

        public void RestoreProblemState(IProblemDto problemState)
        {
            _problem = (TspProblem)_problemDeserializer.CreateProblemFromDto(problemState);
            var solution = _problem.CurrentSolution;
            ProblemParams = new TspParamsViewModel(_problem.ProblemParams);
            
            UpdateProblemInstante(_problem.ProblemInstance);
            
            ((DelegateCommand)GenerateInstanceCommand).RaiseCanExecuteChanged();
            if (solution != null)
                OnNewSolution(solution);
            if (SelectedAlgorithm != null)
                SelectedAlgorithm.StopSolving();

            if (problemState.AlgorithmName == null)
                return;

            var algorithm = Algorithms.SingleOrDefault(alg => alg.AlgorithmName.Equals(problemState.AlgorithmName));
            if (algorithm == null)
                return;
            SelectedAlgorithm = algorithm;
            var algorithmParams = algorithm.Algorithm.DeserializeParams(problemState.AlgorithmParams);
            SelectedAlgorithm.AlgorithmParams.UpdateParams(algorithmParams);
            SelectedAlgorithm.Algorithm.UpdateParams(algorithmParams);
            SelectedAlgorithm.Algorithm.DeserializeState(problemState.AlgorithmState, _problem.CurrentSolution, _problem.ProblemInstance);

        }
    }
}