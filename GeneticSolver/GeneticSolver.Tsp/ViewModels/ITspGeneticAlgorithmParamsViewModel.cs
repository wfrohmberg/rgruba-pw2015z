﻿using System.Collections.Generic;
using GenericSolver.UI.Common;
using GeneticSolver.Core;
using GeneticSolver.Tsp.Problem;

namespace GeneticSolver.Tsp.ViewModels
{
    public interface ITspGeneticAlgorithmParamsViewModel : IAlgorithmParamsViewModel
    {
        IList<ISelection<TspGraph, BasePath>> Selections { get; }

        ISelection<TspGraph, BasePath> Selection { get; set; }

        IList<ICrossingOperator<TspGraph, BasePath>> CrossingOperators { get; }

        ICrossingOperator<TspGraph, BasePath> Crossing { get; set; }

        int SizeOfPopulation { get; set; }

        int NumberOfGenerations { get; set; }

        double CrossingProbability { get; set; }

        double MutationProbability { get; set; }

        new IGeneticParams<TspGraph, BasePath> AlgorithmParams { get; }
    }
}