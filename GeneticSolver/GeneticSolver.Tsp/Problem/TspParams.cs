﻿using GeneticSolver.Core.Problem;

namespace GeneticSolver.Tsp.Problem
{
    public class TspParams : ITspProblemParams
    {
        public int Top { get; set; }

        public int Bottom { get; set; }
        
        public int Left { get; set; }
        
        public int Right { get; set; }

        public int NumberOfElements { get; set; }
    }

    public interface ITspProblemParams : IProblemParams
    {
       int Top { get; set; }

       int Bottom { get; set; }

       int Left { get; set; }

       int Right { get; set; }

       int NumberOfElements { get; set; }
    }
}