using GeneticSolver.Core.Problem;

namespace GeneticSolver.Tsp.Problem
{
    public class TspSolution : ProblemSolution<TspGraph,BasePath>
    {
        public TspSolution(TspGraph instance, BasePath solutionValue) : base(instance, solutionValue)
        {

        }

        public override void Evaluate()
        {
            SolutionValue.Evaluate();
        }

        protected override int Compare(IProblemSolution<TspGraph, BasePath> other)
        {
            if (SolutionValue.Distance == other.SolutionValue.Distance) return 0;
            return SolutionValue.Distance < other.SolutionValue.Distance ? -1 : 1;
        }

        protected override bool IsBetter(IProblemSolution<TspGraph, BasePath> problemSolution)
        {
            return SolutionValue.Distance < problemSolution.SolutionValue.Distance;
        }
    }
}