﻿using System;
using GeneticSolver.Core.Problem;

namespace GeneticSolver.Tsp.Problem
{
    public class TspGraph : IProblemInstance
    {
        private readonly Point[] _points;

        public Point[] Points
        {
            get { return _points; } 
            
        }

        private readonly int[,] _distances;

        public int[,] Distances
        {
            get { return _distances; }
        }

        public TspGraph(int numberOfPoints)
        {
            _points = new Point[numberOfPoints];
            _distances = new int[numberOfPoints, numberOfPoints];
        }

        public void AddElement(int number, double x, double y)
        {
            _points[number] = new Point(x, y);
        }

        public void CalculateDistances()
        {
            int i = -1;
            foreach (var from in _points)
            {
                i++;
                int j = -1;
                foreach (var to in _points)
                {
                    j++;
                    if (i == j)
                        continue;
                    //TODO do I need sqrt?
                    _distances[i, j] = CalculateDistance(to, @from);
                }
            }
        }

        private static int CalculateDistance(Point to, Point @from)
        {
            return Convert.ToInt32(Math.Sqrt(Math.Pow(to.X - @from.X, 2) + Math.Pow(to.Y - @from.Y, 2)));
        }
    }
}