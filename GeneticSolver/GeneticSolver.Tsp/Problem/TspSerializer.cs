﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Documents;
using GeneticSolver.Core;
using GeneticSolver.Core.Problem;
using Newtonsoft.Json;

namespace GeneticSolver.Tsp.Problem
{
    public class TspSerializer : IProblemSerializer<TspProblem>, IProblemDeserializer<TspProblem>
    {
        public class TspProblemDto : IProblemDto
        {
            public TspParams ProblemParams { get; set; }

            public Point[] Points { get; set; }

            public IList<int> Vertexes { get; set; }
            
            public string AlgorithmName { get; set; }

            public string AlgorithmParams { get; set; }
            
            public string AlgorithmState { get; set; }

        }

        public string Serialize(TspProblem problem, IAlgorithm algorithm, IAlgorithmParams algorithmParams)
        {
           
            var dto = new TspProblemDto
            {
                ProblemParams = (TspParams)problem.ProblemParams,
                Points = ((TspGraph)problem.ProblemInstance).Points,
                Vertexes = problem.CurrentSolution != null ? ((BasePath)problem.CurrentSolution.SolutionValue).Vertexes : null,
                AlgorithmName = algorithm != null ? algorithm.AlgorithmName : string.Empty,
                AlgorithmParams = algorithm != null ? algorithm.SerializeParams(algorithmParams) : string.Empty,
                AlgorithmState = algorithm != null ? algorithm.SerializeState() : string.Empty,
            };

            var serializeObject = JsonConvert.SerializeObject(dto);
            return serializeObject;
        }

        public IProblemDto Deserialize(string jsonString)
        {
            return JsonConvert.DeserializeObject<TspProblemDto>(jsonString); ;
        }

        public IProblem CreateProblemFromDto(IProblemDto dto)
        {
            var problemDto = (TspProblemDto) dto;
            var problemInstance = new TspGraph(problemDto.Points.Length);
            for (int i = 0; i < problemDto.Points.Length; i++)
            {
                problemInstance.AddElement(i, problemDto.Points[i].X, problemDto.Points[i].Y);
            }
            problemInstance.CalculateDistances();

            var problem = new TspProblem();
            problem.ProblemParams = problemDto.ProblemParams;
            problem.ProblemInstance = problemInstance;

            if (problemDto.Vertexes != null)
            {
                var path = new Path(problemInstance);
                var solution = new TspSolution(problemInstance, path);

                for (var i = 0; i < problemDto.Vertexes.Count; i++)
                {
                    path.Vertexes.Add(problemDto.Vertexes[i]);
                }
                problem.CurrentSolution = solution;
            }

            return problem;
        }
    }
}