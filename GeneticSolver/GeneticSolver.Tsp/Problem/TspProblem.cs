﻿using System.Windows.Controls;
using GenericSolver.UI.Common;
using GeneticSolver.Core.Problem;

namespace GeneticSolver.Tsp.Problem
{
    public class TspProblem : ProblemBase, ITspProblem
    {
        private readonly IProblemInstanceGenerator<TspGraph> _generator;
        private readonly IDrawingService<TspGraph,BasePath> _drawingService;

        public TspProblem() :base("Problem Komiwojażera")
        {
            _generator = new TspInstanceGenerator();
            _drawingService = new TspDrawingService();
            ProblemParams = new TspParams();
        }

        protected override IProblemInstance GenerateInstance(IProblemParams problemParams)
        {
            return _generator.Generate(problemParams);
        }

        public override void DrawInstance(IProblemInstance instance, Canvas drawingArea)
        {
            _drawingService.DrawInstance((TspGraph)instance ,drawingArea);
        }

        public override void DrawSolution(IProblemSolution solution, Canvas drawingArea)
        {
            _drawingService.DrawSolution((IProblemSolution<TspGraph, BasePath>) solution,drawingArea);
        }

        public ITspProblemParams ProblemParams { get; set; }

    }

    public interface ITspProblem
    {
        ITspProblemParams ProblemParams { get; set; }
    }
}