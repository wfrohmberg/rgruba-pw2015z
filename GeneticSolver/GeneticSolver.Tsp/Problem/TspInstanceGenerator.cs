﻿using System;
using GeneticSolver.Core.Problem;

namespace GeneticSolver.Tsp.Problem
{
    public class TspInstanceGenerator : IProblemInstanceGenerator<TspGraph>
    {
        static readonly Random XRandom = new Random(9);
        static readonly Random YRandom = new Random(11);

        public TspGraph Generate(IProblemParams parameters)
        {
            var par = parameters as TspParams;
            if (par == null)
                throw new ArgumentException("Invalid argument type", "parameters");

            var graph = new TspGraph(par.NumberOfElements);
            for (int i = 0; i < par.NumberOfElements; i++)
            {
                graph.AddElement(i, XRandom.Next(par.Left, par.Right), YRandom.Next(par.Top, par.Bottom));

            }
            graph.CalculateDistances();
            return graph;
        }
    }
}