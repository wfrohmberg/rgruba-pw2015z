﻿using System.Collections.Generic;
using GeneticSolver.Tsp.Problem;

namespace GeneticSolver.Tsp
{
    public class Path : BasePath
    {
        public Path(TspGraph graph)
        {
            Graph = graph;
            Vertexes = new List<int>(graph.Points.Length);
        }

    }

    public class EmptyPath : Path
    {
        public EmptyPath(TspGraph graph) : base(graph)
        {
            for (int i = 0; i < graph.Points.Length; i++)
            {
                Vertexes.Add(0);
            }
        }
    }
}