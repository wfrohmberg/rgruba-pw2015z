﻿using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using GenericSolver.UI.Common;
using GeneticSolver.Core.Problem;
using GeneticSolver.Tsp.Problem;

namespace GeneticSolver.Tsp
{
    public class TspDrawingService : IDrawingService<TspGraph,BasePath>
    {
        public void DrawInstance(TspGraph instance, Canvas drawingArea)
        {
            drawingArea.Children.Clear();
            foreach (var element in instance.Points)
            {
                var rec = new Rectangle();
                Canvas.SetTop(rec, element.Y);
                Canvas.SetLeft(rec, element.X);
                rec.Width = 3;
                rec.Height = 3;
                rec.Fill = new SolidColorBrush(Colors.Red);
                drawingArea.Children.Add(rec);
            }
        }

        public void DrawSolution(IProblemSolution<TspGraph, BasePath> solution, Canvas drawingArea)
        {
            drawingArea.Children.Clear();
            var blackBrush = new SolidColorBrush {Color = Colors.Black};
            var polygonPoints = new PointCollection();
            var yellowPolyline = new Polyline {Stroke = blackBrush, StrokeThickness = 4};


            foreach (var source in solution.SolutionValue.Vertexes)
            {
                if (source == -1) continue;

                var element = solution.Instance.Points[source];

                polygonPoints.Add(new System.Windows.Point(element.X, element.Y));
            }
            yellowPolyline.Points = polygonPoints;
            drawingArea.Children.Add(yellowPolyline);
        }
    }
}