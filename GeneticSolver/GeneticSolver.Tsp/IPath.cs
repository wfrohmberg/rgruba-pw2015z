﻿using System.Collections.Generic;
using GeneticSolver.Tsp.Problem;
using Newtonsoft.Json;

namespace GeneticSolver.Tsp
{
    public interface IPath
    {
        IList<int> Vertexes { get; set; }

        [JsonIgnore]
        int Distance { get; }
        
        [JsonIgnore]
        TspGraph Graph { get; }

        void Evaluate();
    }

    public class BasePath : IPath
    {
        public IList<int> Vertexes { get; set; }

        public TspGraph Graph { get; protected set; }

        private int? _distance;

        public int Distance
        {
            get
            {
                if (_distance == null)
                {
                    _distance = CalculateDistance();
                }
                return _distance.Value;
            }
        }

        public void Evaluate()
        {
            _distance = CalculateDistance();
        }

        private int CalculateDistance()
        {
            int sum = 0;
            for (int i = 0; i < Vertexes.Count - 1; i++)
            {
                var from = Vertexes[i];
                var to = Vertexes[i + 1];
                sum += Graph.Distances[from, to];
            }
            return sum;
        }
    }
}