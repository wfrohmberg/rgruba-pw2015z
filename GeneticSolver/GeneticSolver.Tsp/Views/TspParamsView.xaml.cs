﻿using System.Windows.Controls;

namespace GeneticSolver.Tsp.Views
{
    /// <summary>
    /// Interaction logic for TspParamsView.xaml
    /// </summary>
    public partial class TspParamsView : UserControl
    {
        public TspParamsView()
        {
            InitializeComponent();
        }
    }
}
