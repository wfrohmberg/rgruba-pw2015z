﻿using System.Windows.Controls;

namespace GeneticSolver.Tsp.Views
{
    /// <summary>
    /// Interaction logic for TspProblemView.xaml
    /// </summary>
    public partial class TspProblemView : UserControl
    {
        public TspProblemView()
        {
            InitializeComponent();
        }
    }
}
